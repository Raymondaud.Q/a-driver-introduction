#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/cdev.h>
#include <linux/fs.h>
#include <linux/slab.h>
#include <linux/uaccess.h>

#define LICENCE     "Proprietary"
#define AUTEUR      "Tristan Marrec for Epic Games"
#define DESCRIPTION "TP2 Couche Basse"
#define DEVICE      "Os a moelle"

/* USAGE 
insmod tp2.ko
mknod /dev/sm c <Major> <Minor>
echo "txt" > /dev/sm
cat /dev/sm
rm -f /dev/sm
echo pour wrtite
cat pour lire
rmmod tp2
*/

// Taille du buffer
#define N 4

// Exemple de buffer avec une taille de 4 :
// Etapes du write de "123456" : [1,2,3,4] -> [5,6,3,4]
//                                |     |        | |
//                  curseurs ->   |     ----fin--- |
//                           ->   ------debut-------
//
// Read du buffer résultant ([5,6,3,4]) :
// Départ du curseur de début jusqu'a celui de fin avec retour
// au debut si dépassement d'indice -> 3456
//
// (L'exemple ici utilise echo -n "123456" pour ne pas avoir
// le caractere de fin de ligne)

typedef struct {
    bool open;
    unsigned long long end; // Curseur du début du buffer
    unsigned long long start; // Curseur de fin du buffer
    char buffer[N];
} data_s; 

data_s data;

// Vide le buffer
void clear_buffer(void) {
    data.end = 0;
    data.start = 1;
}

static int open_tp(struct inode *inode, struct file *file) {
    data.open = true;
    return 0;
}

static ssize_t read_tp(struct file *f, char *buf, size_t size, loff_t *offset) {
    unsigned long long to_read = 0;

	// Si le buffer viens d'etre vidé, la lecture est finie
    if (data.end == 0) {
        return 0;
    }

	// Nombre d'octet a lire avant la fin du buffer (avec un curseur de fin) ou
	// avant le dernier indice du tableau buffer
	to_read = data.start > data.end ? N-data.start+1 : data.end-data.start+1;

	// Essais de lire le nombre d'octet prévu avec un maximum du size en parametre
    if (copy_to_user(buf, data.buffer+(data.start-1), to_read > size? size : to_read) == 0) {
		// Si tentative de lire plus que la limite size en parametre
        if (to_read > size) {
			// Décallage du curseur de lecture
            data.start += size;
            return size;
        } else {
            if (data.start > data.end && to_read <= size) {
				// Fin de l'indice du buffer, continue
				// la lecture a partir du premier indice
                data.start = 1; 
            } else {
				// Fin de lecture
                clear_buffer();
            }
        }
    } else {
        return -EFAULT;
    }

	// Retourne la taille qui a pu etre lu
    return to_read;
}

static ssize_t write_tp(struct file *f, const char *buf, size_t size, loff_t *offset) {
    unsigned long long empty = 0;
    unsigned long long copied = 0;

    // Si premier write (viens d'être ouvert)
    // alors le buffer est vidé car écriture destructice
    if (data.open) {
        clear_buffer();
        data.open = false;
    }
    
    // empty = la taille maximum qu'il peut etre lu dans le buffer mais bornée par
    // le size en parametre du read
    empty = N - data.end;
    if (empty == 0) {
        data.start = ((data.end + size)%N)+1;
        empty = N;
    }
	empty = empty > size? size : empty;
	copied = empty-copy_from_user(data.buffer, buf, (size_t)empty);
    data.end = empty;

	// Retourne la taille qui a pu être copiée
    return copied;
}
  
dev_t dev;
struct cdev *my_cdev = NULL;
struct file_operations fops = {
    .owner = THIS_MODULE,
    .read = read_tp,
    .write = write_tp,
    .open = open_tp
};

int register_driver(void) {
    if (alloc_chrdev_region(&dev, 0, 1, "TP2") == -1) {
        printk(KERN_ALERT ">>> ERROR alloc_chrdev_region\n");
        return -1;
    }

    printk(KERN_ALERT "Init allocated (major, minor) = (%d,%d)\n", MAJOR(dev), MINOR(dev));

    my_cdev = cdev_alloc();
    if (my_cdev == NULL) {
        printk(KERN_ALERT ">>> ERROR cdev_alloc\n");
        return -2;
    }
    my_cdev->ops = &fops;
    my_cdev->owner = THIS_MODULE;

    if (cdev_add(my_cdev, dev, 1) < 0) {
        printk(KERN_ALERT ">>> ERROR cdev_add\n");
        return -3;
    }
    return 0;
}

static int init(void) {
    printk(KERN_ALERT "TP2 INITIALIZED!\n");
    if (register_driver() != 0) {
        return -1;
    }
    data.end = 0;
    data.start = 1;
    data.open = false;
    return 0;
}

static void exitt(void) {
    printk(KERN_ALERT "GOODBYE TP2 :(\n");
    unregister_chrdev_region(dev, 1);
    cdev_del(my_cdev);
}

MODULE_LICENSE(LICENCE);
MODULE_AUTHOR(AUTEUR);
MODULE_DESCRIPTION(DESCRIPTION);
MODULE_SUPPORTED_DEVICE(DEVICE);

module_init(init);
module_exit(exitt);
