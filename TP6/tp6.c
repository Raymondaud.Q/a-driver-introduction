// RAYMONDAUD Quentin
// m1igai13

#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/cdev.h>
#include <linux/fs.h>
#include <linux/slab.h>
#include <linux/uaccess.h>
#include <linux/list.h>

#define LICENCE     "GPL"
#define AUTEUR      "RAYMONDAUD Quentin"
#define DESCRIPTION "TP6"
#define DEVICE      "Linux menthe"
#define MIN(a, b)   (((a) < (b)) ? (a) : (b))

// UPDATE depuis TP5 : - Allocation troisième mineur
//					   - Gestion des fops à l'ouverture ( Read Destru , Read Non-Destru , Write )
// 					   - Correction : désallocation des régions à la suppression du module ( J'avais oublié de désalloué la deuxième dans mon rendu précédent )

/* USAGE

ex avec echo :

    make
    insmod tp6.ko
    mknod /dev/sr c 243 0 	     // Point d'entrée en lecture destructrice uniquement
    mknod /dev/srn c 243 1		 // ______________ en lecture non-destructrice uniquement
    mknod /dev/sw c 243 2 		 // ______________ en écriture uniquement


    echo " Okeye " > /dev/sw
 	cat /dev/srn   				// AFFICHE Okeye
 	cat /dev/sr 				// AFFICHE Okeye et supprime
 	cat /dev/srn  			    // AFFICHE Rien car vide

    rm -f /dev/sw
    rm -f /dev/sr
    rmmod tp6

ex dd :

    make
    insmod tp6.ko
    mknod /dev/sr c 243 0 	     // Point d'entrée en lecture destructrice uniquement
    mknod /dev/srn c 243 1		 // ______________ en lecture non-destructrice uniquement
    mknod /dev/sw c 243 2 		 // ______________ en écriture uniquement
    
    echo " SALUTATION INVOCATEUR, BIENVENU DANS LA FAILLE DE LINVOCATEUR" > /dev/sw
    cat /dev/srn
    touch dest.txt
    dd bs=3 if=/dev/srn of=dest.txt count=1
    cat dest.txt 
    dd bs=3 if=/dev/srn of=dest.txt count=1
    cat dest.txt 
    dd bs=3 if=/dev/srn of=dest.txt count=1
    cat dest.txt 
    dd bs=3 if=/dev/srn of=dest.txt count=1
    cat dest.txt 
    dd bs=3 if=/dev/srn of=dest.txt count=1
    cat dest.txt 
    dd bs=3 if=/dev/srn of=dest.txt count=1
    cat dest.txt 
    dd bs=3 if=/dev/srn of=dest.txt count=1
    cat dest.txt 
    dd bs=3 if=/dev/srn of=dest.txt count=1
    cat dest.txt 
    dd bs=3 if=/dev/srn of=dest.txt count=1
    cat dest.txt 
    dd bs=3 if=/dev/srn of=dest.txt count=1
    cat dest.txt 
    dd bs=3 if=/dev/srn of=dest.txt count=1
    cat dest.txt 
    dd bs=3 if=/dev/srn of=dest.txt count=1
    cat dest.txt 
    dd bs=3 if=/dev/srn of=dest.txt count=1
    cat dest.txt 
    dd bs=3 if=/dev/srn of=dest.txt count=1
    cat dest.txt 
    dd bs=3 if=/dev/srn of=dest.txt count=1
    cat dest.txt 
    dd bs=3 if=/dev/srn of=dest.txt count=1
    cat dest.txt 
    dd bs=3 if=/dev/srn of=dest.txt count=1
    cat dest.txt 
    dd bs=3 if=/dev/srn of=dest.txt count=1
    cat dest.txt 
    dd bs=3 if=/dev/srn of=dest.txt count=1
    cat dest.txt 
    dd bs=3 if=/dev/srn of=dest.txt count=1
    cat dest.txt 
    dd bs=3 if=/dev/srn of=dest.txt count=1
    cat dest.txt 
    dd bs=3 if=/dev/srn of=dest.txt count=1
    cat dest.txt 
    dd bs=3 if=/dev/srn of=dest.txt count=1
    cat dest.txt 
    dd bs=3 if=/dev/srn of=dest.txt count=1
    cat dest.txt 
    dd bs=3 if=/dev/srn of=dest.txt count=1
    cat dest.txt 
    dd bs=3 if=/dev/srn of=dest.txt count=1
    cat dest.txt 
    dd bs=3 if=/dev/srn of=dest.txt count=1
    cat dest.txt 
    dd bs=3 if=/dev/srn of=dest.txt count=1
    cat dest.txt 
    dd bs=3 if=/dev/srn of=dest.txt count=1
    cat dest.txt 
    dd bs=3 if=/dev/srn of=dest.txt count=1
    cat dest.txt 
    dd bs=3 if=/dev/srn of=dest.txt count=1
    cat dest.txt 
    dd bs=3 if=/dev/srn of=dest.txt count=1
    cat dest.txt 
    dd bs=3 if=/dev/srn of=dest.txt count=1
    cat dest.txt 
    dd bs=3 if=/dev/srn of=dest.txt count=1
    cat dest.txt 
    dd bs=3 if=/dev/srn of=dest.txt count=1
    cat dest.txt 
    dd bs=3 if=/dev/srn of=dest.txt count=1
    cat dest.txt 
    dd bs=3 if=/dev/srn of=dest.txt count=1
    cat dest.txt 
    dd bs=3 if=/dev/srn of=dest.txt count=1
    cat dest.txt 
    dd bs=3 if=/dev/srn of=dest.txt count=1
    cat dest.txt 
    dd bs=3 if=/dev/srn of=dest.txt count=1
    cat dest.txt 
    dd bs=3 if=/dev/srn of=dest.txt count=1
    cat dest.txt 
    dd bs=3 if=/dev/srn of=dest.txt count=1
    cat dest.txt 
    dd bs=3 if=/dev/srn of=dest.txt count=1
    cat dest.txt 
    dd bs=3 if=/dev/srn of=dest.txt count=1
    cat dest.txt 
    dd bs=3 if=/dev/srn of=dest.txt count=1
    cat dest.txt 
    dd bs=3 if=/dev/srn of=dest.txt count=1
    cat dest.txt 
    dd bs=3 if=/dev/sr of=dest.txt count=1
    cat dest.txt 
    dd bs=3 if=/dev/sr of=dest.txt count=1
    cat dest.txt 
    dd bs=3 if=/dev/sr of=dest.txt count=1
    cat dest.txt 
    dd bs=3 if=/dev/sr of=dest.txt count=1
    cat dest.txt 
    dd bs=3 if=/dev/sr of=dest.txt count=1
    cat dest.txt 
    dd bs=3 if=/dev/sr of=dest.txt count=1
    cat dest.txt 
    dd bs=3 if=/dev/sr of=dest.txt count=1
    cat dest.txt 
    dd bs=3 if=/dev/sr of=dest.txt count=1
    cat dest.txt 
    dd bs=3 if=/dev/sr of=dest.txt count=1
    cat dest.txt 
    dd bs=3 if=/dev/sr of=dest.txt count=1
    cat dest.txt 
    dd bs=3 if=/dev/sr of=dest.txt count=1
    cat dest.txt 
    dd bs=3 if=/dev/sr of=dest.txt count=1
    cat dest.txt 
    dd bs=3 if=/dev/sr of=dest.txt count=1
    cat dest.txt 
    dd bs=3 if=/dev/sr of=dest.txt count=1
    cat dest.txt 

    
    rm -f /dev/sr
    rm -f /dev/srn
    rm -f /dev/sw
    
    rmmod tp6

*/




struct BufferList{
    char * maDonnee;
    ssize_t nb_donnes;
    struct list_head liste;
};

ssize_t data_read = 0;
struct list_head maListe;
struct file_operations fops;
dev_t dev;
struct cdev *my_cdev = NULL;

static ssize_t read(struct file *f, char *buf, size_t size, loff_t *offset) {
    struct BufferList *unElement;
    ssize_t size_to_copy;
    ssize_t size_not_copied;
    printk(KERN_INFO " READ called \n");
    if ( list_empty(&maListe) )
        return 0;
    unElement = list_entry(maListe.next, struct BufferList, liste);
    size_to_copy = MIN(size, unElement->nb_donnes - data_read );
    size_not_copied = copy_to_user(buf, unElement->maDonnee + data_read , size_to_copy * sizeof (char));
    data_read += size_to_copy - size_not_copied;
    printk(KERN_INFO " stc %ld | snc  %ld \n" , size_to_copy , size_not_copied  );
    if ( data_read == unElement->nb_donnes){
        printk(KERN_INFO " dr %ld \n" , data_read  );
        data_read = 0;
        list_del(&unElement->liste);
        kfree(unElement->maDonnee);
        kfree(unElement);
    }
    return size_to_copy - size_not_copied;
}

static ssize_t readn(struct file *f, char *buf, size_t size, loff_t *offset) {
    struct BufferList *unElement;
    ssize_t size_to_copy;
    ssize_t size_not_copied;
    printk(KERN_INFO " READ called \n");
    if ( list_empty(&maListe)  )
        return 0;
    unElement = list_entry(maListe.next, struct BufferList, liste);
    
    size_to_copy = MIN(size, unElement->nb_donnes - data_read );
    size_not_copied = copy_to_user(buf, unElement->maDonnee + data_read , size_to_copy * sizeof (char));
    data_read += size_to_copy - size_not_copied;
    if ( data_read == unElement->nb_donnes){
        printk(KERN_INFO " dr %ld \n" , data_read  );
        data_read = size_to_copy - size_not_copied;
    }
    return size_to_copy - size_not_copied;
}

static ssize_t write(struct file *filp, 
                const char __user *buf, size_t len, loff_t *off)
{
    struct BufferList *nouveauCraquage;
    int data_not_copied = 0;
    printk(KERN_INFO "Write Function CALLED \n");
    nouveauCraquage = (struct BufferList *)kmalloc(sizeof(struct BufferList),GFP_KERNEL);
    nouveauCraquage->maDonnee = (char*) kmalloc(sizeof(char) * len, GFP_KERNEL);
    list_add_tail(&nouveauCraquage->liste, &maListe);
    data_not_copied = copy_from_user(nouveauCraquage->maDonnee, buf, len);
    nouveauCraquage->nb_donnes = len - data_not_copied;
    printk(KERN_INFO " Ajout de %ld char\n", nouveauCraquage->nb_donnes);
    if (data_not_copied != 0) {
        krealloc(nouveauCraquage->maDonnee, nouveauCraquage->nb_donnes, GFP_KERNEL);
    }
    return nouveauCraquage->nb_donnes;
}



static int open(struct inode *inode, struct file *file) {
    
    int minor = MINOR(inode->i_rdev);
    printk(KERN_INFO "Device File Opened...!!! %i \n", minor );
    
    if ( minor == 0 ) { // Destructive READ ONLY
        printk(KERN_INFO "LECTURE DESTRUCTRICE\n");
        fops.read=read;
        fops.write=NULL;
    }
    else if ( minor == 1) { // READ ONLY
    	printk(KERN_INFO "LECTURE NON-DESTRUCTRICE\n");
    	fops.read=readn;
        fops.write=NULL;
    }

    else {              // WRITE ONLY
        printk(KERN_INFO "ECRITURE\n");
        fops.write=write;
        fops.read=NULL;
    }
    
    return 0;
}


struct file_operations fops = {
    .owner = THIS_MODULE,
    .read = read,
    .write = write,
    .open = open
};
 
int register_driver(void) {
    if (alloc_chrdev_region(&dev, 0, 3, "TP6") == -1) {
        printk(KERN_ALERT ">>> ERROR alloc_chrdev_region\n");
        return -1;
    }

    printk(KERN_ALERT "Init allocated (major, minor) = (%d,%d)\n", MAJOR(dev), MINOR(dev));

    my_cdev = cdev_alloc();
    if (my_cdev == NULL) {
        printk(KERN_ALERT ">>> ERROR cdev_alloc\n");
        return -2;
    }
    my_cdev->ops = &fops;
    my_cdev->owner = THIS_MODULE;

    if (cdev_add(my_cdev, dev, 3) < 0) {
        printk(KERN_ALERT ">>> ERROR cdev_add\n");
        return -3;
    }


    return 0;
}

static int d_init(void) {
    printk(KERN_ALERT " OH NON PAS LE TP6 !\n");
    INIT_LIST_HEAD(&maListe);
    if (register_driver() != 0) {
        return -1;
    }
    return 0;
}

static void d_exit(void) {
    struct BufferList *unElement;
    struct list_head *pos, *q;
    printk(KERN_ALERT " ALLEE SALUTTTTT TP6 :D \n");
    list_for_each_safe(pos, q, &maListe){
        unElement = list_entry(pos, struct BufferList, liste);
        printk(KERN_INFO " Remove Elem, %ld char\n" ,unElement->nb_donnes);
        list_del(pos);
        kfree(unElement->maDonnee);
        kfree(unElement);
    }
    unregister_chrdev_region(dev, 3);
    cdev_del(my_cdev);
}

MODULE_LICENSE(LICENCE);
MODULE_AUTHOR(AUTEUR);
MODULE_DESCRIPTION(DESCRIPTION);
MODULE_SUPPORTED_DEVICE(DEVICE);

module_init(d_init);
module_exit(d_exit);
