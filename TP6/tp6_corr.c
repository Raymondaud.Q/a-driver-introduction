/*
Nom : Florian
Prenom : Boisnon
Compte examen : m1info9
*/

/**
insmod tp3.ko
rm -f /dev/sm
mknod /dev/sm c 248 0
echo "un message" > /dev/sm
cat /dev/sm
**/

// minor 0 : ecriture mais pas lecture
// minor 1 : pas ecriture mais lecture destructive
// minor 2 : lecture non destructive

/* Includes */
#include <linux/init.h>
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/kernel.h>
#include <linux/cdev.h>
#include <linux/kdev_t.h>
#include <linux/fs.h>
#include <asm/uaccess.h>
#include <linux/mutex.h>
#include <linux/device.h>
#include <linux/slab.h>
#include <linux/list.h>

#define MIN(a,b) (((a)<(b))?(a):(b))

// Char driver functions
static ssize_t sample_read(struct file *f, char *buf, size_t size, loff_t *offset);
static ssize_t sample_write(struct file *f, const char *buf, size_t size, loff_t *offset);
static int sample_open(struct inode *in, struct file *f);
static int sample_release(struct inode *in, struct file *f);

// standard file_ops for char driver
static struct file_operations fops =
{
  .read = sample_read,
  .write = sample_write,
  .open = sample_open,
  .release = sample_release,
};

unsigned int maxFile= 3;
int debug = 0;
module_param(debug,int,S_IRUGO);


struct myStructure {
  char* my_data;
  unsigned int size_my_data;
  struct list_head node;
};

// The dev_t for our device
dev_t dev;
// Initialisation
bool open = false; //fichier ouvert ou non
struct list_head head; //tete de liste

// The cdev for our device
struct cdev *my_cdev;

static ssize_t sample_read(struct file *f, char *buf, size_t size, loff_t *offset)
{
  unsigned int size_to_read;
  unsigned int size_unread;
  struct myStructure *varStrct;
  struct list_head *varLH;

  printk(KERN_ALERT "Read called!\n");

  if(list_empty(&head)){
    printk(KERN_ALERT "Empty!\n");
    return 0;
  }

  varLH = head.next;
  varStrct = list_entry(varLH, struct myStructure, node);

  size_to_read = MIN(size, varStrct->size_my_data);
  size_unread = copy_to_user(buf, varStrct->my_data,size_to_read);
  if (debug){
    printk("varStrct->my_data : %s\n", varStrct->my_data);
    printk("size_to_read = %d\n", size_to_read);
    printk("size_unread = %d\n", size_unread);
  }

  list_del(&varStrct->node);
  kfree(varStrct->my_data);
  kfree(varStrct);
  return size_to_read;
}

struct list_head saveHead;

static ssize_t sample_read_no_dest(struct file *f, char *buf, size_t size, loff_t *offset)
{
  unsigned int size_to_read;
  unsigned int size_unread;
  struct myStructure *varStrct;
  struct list_head *varLH;

  printk(KERN_ALERT "Read called!\n");

  if(list_empty(&head)){
    printk(KERN_ALERT "Empty!\n");
    head = saveHead;
    return 0;
  }

  varLH = head.next;
  varStrct = list_entry(varLH, struct myStructure, node);

  size_to_read = MIN(size, varStrct->size_my_data);
  size_unread = copy_to_user(buf, varStrct->my_data,size_to_read);
  if (debug){
    printk("varStrct->my_data : %s\n", varStrct->my_data);
    printk("size_to_read = %d\n", size_to_read);
    printk("size_unread = %d\n", size_unread);
  }

  head = varStrct->node;

  return size_to_read;
}

// Ne lis que le premier noeud
/**ssize_t size_read = 0;
static ssize_t sample_read_no_dest(struct file *f, char *buf, size_t size, loff_t *offset)
{
  unsigned int size_to_read;
  unsigned int size_unread;
  struct myStructure *varStrct;
  struct list_head *varLH;

  printk(KERN_ALERT "Read called!\n");

  if(list_empty(&head)){
    printk(KERN_ALERT "Empty!\n");
    return 0;
  }

  varLH = head.next;
  varStrct = list_entry(varLH, struct myStructure, node);

  size_to_read = MIN(size, varStrct->size_my_data-size_read);
  size_unread = copy_to_user(buf, varStrct->my_data+size_read,size_to_read* sizeof (char));
  size_read += size_to_read -size_unread;

  if (debug){
    printk("varStrct->my_data : %s\n", varStrct->my_data);
    printk("size_to_read = %d\n", size_to_read);
    printk("size_unread = %d\n", size_unread);
    //printk("size_read = %d\n", size_read);
  }

  size_read += size_to_read - size_unread;
  if ( size_read == varStrct->size_my_data){
      size_read = size_to_read - size_unread;
  }
  return size_to_read - size_unread;
}
**/

static ssize_t sample_write(struct file *f, const char *buf, size_t size, loff_t *offset)
{
  struct myStructure *varStrct;
  //struct list_head *tmp;
  //struct list_head *varLH;
  unsigned int size_to_copy;
  unsigned int size_uncopied;

  printk(KERN_ALERT "Write called!\n");

  /**if (open) {
      open = false;
      list_for_each_safe(varLH, tmp, &head) {
          varStrct = list_entry(varLH, struct myStructure, node);
          kfree(varStrct->my_data);
          kfree(varStrct);
          list_del(varLH);
      }
  }**/

  varStrct = kmalloc(sizeof(struct myStructure), GFP_KERNEL);
  INIT_LIST_HEAD(&varStrct->node);
  list_add_tail(&varStrct->node,&head);
  varStrct->size_my_data = 0;

  size_to_copy = size;

  varStrct->my_data = kmalloc(size_to_copy * sizeof(char) +1, GFP_KERNEL); //+1 car le string doit se finir par /n et /0 ligne 140
  size_uncopied = size - size_to_copy;
  if (copy_from_user(varStrct->my_data, buf, size_to_copy) != 0) {
      printk("Copie non entiere.\n");
  }
  varStrct->my_data[size_to_copy]='\0';

  varStrct->size_my_data=size_to_copy;

  if (debug){
    printk("size_to_copy = %d\n", size_to_copy);
    printk("size_uncopied = %d\n", size_uncopied);
      printk("varStrct->my_data : %s\n", varStrct->my_data);
  }

  return size_to_copy;

}

  static int sample_open(struct inode *in, struct file *f)
  {
    printk(KERN_ALERT "Open called!\n");
    open = true;
    if (MINOR( in->i_rdev) == 1){
      printk(KERN_ALERT "Read / no Write!\n");
      fops.read = sample_read;
      fops.write = NULL;
    }
    else if (MINOR( in->i_rdev) == 0){
      printk(KERN_ALERT "no Read / Write!\n");
      fops.read = NULL;
      fops.write = sample_write;
    }
    else if (MINOR( in->i_rdev) == 2){
      printk(KERN_ALERT "Read no Dest / no Write!\n");
      saveHead = head;
      fops.read = sample_read_no_dest;
      fops.write = NULL;
    }
    else{
      printk(KERN_ALERT "sampel_open Else\n");
    }

    return 0;
  }

static int sample_release(struct inode *in, struct file *f)
{
  return 0;
}

int sample_init(void)
{
  INIT_LIST_HEAD(&head);

	// Dynamic allocation for (major,minor)
	if (alloc_chrdev_region(&dev,0,maxFile,"sample") == -1)
	{
		printk(KERN_ALERT ">>> ERROR alloc_chrdev_region\n");
		return -EINVAL;
	}
	// Print out the values
	printk(KERN_ALERT "Init allocated (major, minor)=(%d,%d)\n",MAJOR(dev),MINOR(dev));//premier
  printk(KERN_ALERT "Init allocated (major, minor)=(%d,%d)\n",MAJOR(dev),MINOR(dev)+maxFile-1);//dernier


	// Structures allocation
	my_cdev = cdev_alloc();
	my_cdev->ops = &fops;
	my_cdev->owner = THIS_MODULE;
	// linking operations to device
	cdev_add(my_cdev,dev,maxFile);

	return(0);
}


static void sample_cleanup(void)
{
  printk(KERN_ALERT "Goodbye\n");
	// Unregister
	unregister_chrdev_region(dev,maxFile);
	// Delete cdev
	cdev_del(my_cdev);
}

module_exit(sample_cleanup);
module_init(sample_init);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("florian boisnon");
MODULE_DESCRIPTION("tp2");
MODULE_SUPPORTED_DEVICE("MyDevice");
