// RAYMONDAUD Quentin
// m1igai13

#include <linux/init.h>
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/kernel.h>
#include <linux/cdev.h>
#include <linux/kdev_t.h>
#include <linux/fs.h>
#include <linux/uaccess.h>
#include <linux/mutex.h>
#include <linux/device.h>
#include <linux/slab.h>
#include <linux/list.h>
#include <linux/unistd.h>
#include <linux/sched.h>

#define LICENCE     "GPL"
#define AUTEUR      "RAYMONDAUD Quentin"
#define DESCRIPTION "TP7"
#define DEVICE      "Linux menthe"
#define MIN(a, b)   (((a) < (b)) ? (a) : (b))

/*

rm -f /dev/sr
rm -f /dev/srn
rm -f /dev/sw
rmmod tp7
make
insmod tp7.ko
mknod /dev/sr c 240 0 
mknod /dev/srn c 240 1
mknod /dev/sw c 240 2
python test.py

*/

static ssize_t read(struct file *f, char *buf, size_t size, loff_t *offset);
static ssize_t write(struct file *f, const char *buf, size_t size, loff_t *offset);
static int sample_open(struct inode *in, struct file *f);
static int release(struct inode *in, struct file *f);

// file_ops for char driver
static struct file_operations fops =
{
  .read = read,
  .write = write,
  .open = sample_open,
  .release = release,
};

unsigned int maxFile= 3;
int debug = 0;
module_param(debug,int,S_IRUGO);

struct List_BufferList{
	struct BufferList * BufferList;
	ssize_t nbList;
	struct list_head node;
	struct list_head listlisthead;
	int pid;
};

struct BufferList {
  char* my_data;
  unsigned int size_my_data;
  struct list_head node;
};

struct List_BufferList * listBuffList;
dev_t dev;
bool open = false; // ouvert ou non
struct list_head head; // tete de liste
struct list_head listHead;
struct cdev *my_cdev;

static ssize_t read(struct file *f, char *buf, size_t size, loff_t *offset) {
	unsigned int sizeToRead;
	unsigned int sizeNotRead;
	struct BufferList *bufList;
	struct list_head * headList;

	printk(KERN_ALERT "Read called!\n");

	if(list_empty(&head)){
		printk(KERN_ALERT "Empty!\n");
		return 0;
	}

	headList = head.next;
	bufList = list_entry( headList, struct BufferList, node);

	sizeToRead = MIN(size, bufList->size_my_data);
	sizeNotRead = copy_to_user(buf, bufList->my_data,sizeToRead);
	if (debug){
		printk("bufList->my_data : %s\n", bufList->my_data);
		printk("sizeToRead | sizeNotRead = %d | %d\n ", sizeToRead, sizeNotRead);
	}

	list_del(&bufList->node);
	kfree(bufList->my_data);
	kfree(bufList);
	return sizeToRead;
}

struct list_head saveHead;

static ssize_t readN(struct file *f, char *buf, size_t size, loff_t *offset) {
	unsigned int sizeToRead;
	unsigned int sizeNotRead;
	struct BufferList *bufList;
	struct list_head * headList;

	printk(KERN_ALERT "Read called!\n");

	if(list_empty(&head)) {
		printk(KERN_ALERT "Empty!\n");
		head = saveHead;
		return 0;
	}

	 headList = head.next;
	bufList = list_entry( headList, struct BufferList, node);

	sizeToRead = MIN(size, bufList->size_my_data);
	sizeNotRead = copy_to_user(buf, bufList->my_data,sizeToRead);

	if (debug){
		printk("bufList->my_data : %s\n", bufList->my_data);
		printk("sizeToRead | sizeNotRead = %d | %d\n ", sizeToRead, sizeNotRead);
	}

	head = bufList->node;
	return sizeToRead;
}

static ssize_t write(struct file *f, const char *buf, size_t size, loff_t *offset) {
	struct BufferList *bufList;
	unsigned int sizeToCopy;
	unsigned int sizeNotCopied;

	printk(KERN_ALERT "Write called!\n");


	bufList = kmalloc(sizeof(struct BufferList), GFP_KERNEL);
	INIT_LIST_HEAD(&bufList->node);
	list_add_tail(&bufList->node,&head);
	bufList->size_my_data = 0;

	sizeToCopy = size;

	bufList->my_data = kmalloc(sizeToCopy * sizeof(char) +1, GFP_KERNEL); // +1 car le string se termine par /n et /0 ligne 140
	sizeNotCopied = size - sizeToCopy;

	if (copy_from_user(bufList->my_data, buf, sizeToCopy) != 0)
		printk("Copie non entiere.\n");

	bufList->my_data[sizeToCopy]='\0';

	bufList->size_my_data=sizeToCopy;

	if (debug){
		printk("bufList->my_data : %s\n", bufList->my_data);
		printk("sizeToCopy | sizeNotCopied = %d | %d\n ", sizeToCopy, sizeNotCopied);
	}
	return sizeToCopy;
}

static int sample_open(struct inode *in, struct file *f) {

	struct List_BufferList *bufList;
	struct list_head * headList;
	struct list_head *tmp;
	
	printk(KERN_ALERT "Open called!\n");
	printk(KERN_INFO "Process PID : %d\n", current->pid);
	open = true;

	if ( list_empty(&listHead)  ) {
		listBuffList = kmalloc(sizeof(struct List_BufferList), GFP_KERNEL);
		INIT_LIST_HEAD(&listBuffList->node);
		list_add_tail(&listBuffList->node, &listHead);
		listBuffList->pid = current->pid;
		INIT_LIST_HEAD(&head);
		listBuffList->listlisthead=head;
		printk(KERN_INFO "listBuffList->pid : %i\n", listBuffList->pid);
	}

	else {
		bool existe = false;
		list_for_each_safe ( headList, tmp, &listHead) {
			bufList = list_entry( headList, struct List_BufferList, node);
			printk(KERN_INFO "listBuffList->pid : %i\n", bufList->pid);
			if (bufList->pid==current->pid) {
				existe = true;
				head = bufList->listlisthead;
			}
		}

		if (existe)
			printk(KERN_INFO " PID EXISTANT !\n");
		else {
			printk(KERN_INFO " PID INCONNU !\n");
			listBuffList = kmalloc(sizeof(struct List_BufferList), GFP_KERNEL);
			INIT_LIST_HEAD(&listBuffList->node);
			list_add_tail(&listBuffList->node, &listHead);
			listBuffList->pid = current->pid;
			INIT_LIST_HEAD(&head);
			listBuffList->listlisthead=head;
			printk(KERN_INFO " listBuffList->pid : %i\n", listBuffList->pid);
		}
	}

	if (MINOR(in->i_rdev) == 0) {         	// READ
		printk(KERN_ALERT "Read\n");
		fops.read = read;
		fops.write = NULL;
	}
	
	else if (MINOR(in->i_rdev) == 1) {      // READN
		printk(KERN_ALERT "Read_NoD\n");
		saveHead = head;
		fops.read = readN;
		fops.write = NULL;
	}

	else if (MINOR (in->i_rdev) == 2) {     // WRITE
		printk(KERN_ALERT "Write\n");
		fops.read = NULL;
		fops.write = write;
	}

	else 
		printk(KERN_ALERT " Sample dafuq \n");

	return 0;
}

static int release(struct inode *in, struct file *f) {
	listBuffList->listlisthead = head;
	printk(KERN_ALERT "release\n");
	return 0;
}

int d_init (void) {
	INIT_LIST_HEAD(&head);
	INIT_LIST_HEAD(&listHead);
	// Dynamic allocation for (major,minor)
	if (alloc_chrdev_region(&dev,0,maxFile,"sample") == -1) {
		printk(KERN_ALERT ">>> ERROR alloc_chrdev_region\n");
		return -EINVAL;
	}
	// Print out the values
	printk(KERN_ALERT "Init allocated (major, minor)=(%d,%d)\n",MAJOR(dev),MINOR(dev));//premier
	printk(KERN_ALERT "Init allocated (major, minor)=(%d,%d)\n",MAJOR(dev),MINOR(dev)+maxFile-1);//dernier
	// Structures allocation
	my_cdev = cdev_alloc();
	my_cdev->ops = &fops;
	my_cdev->owner = THIS_MODULE;
	// linking operations to device
	cdev_add(my_cdev,dev,maxFile);

	return(0);
}


static void d_exit (void) {
	struct BufferList *varStruct;
	struct list_head *pos, *q;
	struct List_BufferList *varListStrct;
	struct list_head *varLH; //pour foreach
	struct list_head *tmp; //pour foreach

	printk(KERN_ALERT "Allé la bise TP7 ! \n");

	list_for_each_safe(varLH, tmp, &listHead) {
		varListStrct = list_entry(varLH, struct List_BufferList, node);
		head = varListStrct->listlisthead;
		printk(KERN_INFO "PID supprimé : %i\n", varListStrct->pid);
		list_for_each_safe(pos, q, &head){
			varStruct = list_entry(pos, struct BufferList, node);
			printk(KERN_INFO "Remove Elem, %i char\n" ,varStruct->size_my_data);
			list_del(pos);
			kfree(varStruct->my_data);
			kfree(varStruct);
		}
		list_del(varLH);
		kfree(varListStrct);
	}

	// Unregister
	unregister_chrdev_region(dev,maxFile);
	// Delete cdev
	cdev_del(my_cdev);
}

module_exit(d_exit);
module_init(d_init);

MODULE_LICENSE(LICENCE);
MODULE_AUTHOR(AUTEUR);
MODULE_DESCRIPTION(DESCRIPTION);
MODULE_SUPPORTED_DEVICE(DEVICE);
