#include <linux/init.h> 
#include <linux/module.h> 
#include <linux/kernel.h>
#include<linux/moduleparam.h>

// NOOBGUIDE :

// scp HelloK.c root@vm-dyn-0-229:./
// make
// insmod HelloK.ko userMSG="ertgnujhykl"
// rmmod HelloK.ko
// dmesg


#define LICENCE "GPL"
#define AUTEUR "RAYMONDAUD Quentin"
#define DESCRIPTION "Simple Hello Kernel"
#define DEVICE "OS LOUCHE v 175.0.2488"
#define ETC "etc"

static char * userMSG="Hello Kernel !";
module_param(userMSG,charp,S_IRUGO);

static int hello_init(void){
    printk(KERN_ALERT "Hello, Kernel user wants to tell you %s \n",userMSG) ; return 0;
}
static void hello_cleanup(void){
    printk(KERN_ALERT "Goodbye dry kernel!\n");
}


int init_module(void) {
    hello_init();
    return 0;
}
void cleanup_module(void){
    hello_cleanup();
}

MODULE_LICENSE(LICENCE);
MODULE_AUTHOR(AUTEUR);
MODULE_DESCRIPTION(DESCRIPTION);
MODULE_SUPPORTED_DEVICE(DEVICE);
