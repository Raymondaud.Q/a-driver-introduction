// RAYMONDAUD Quentin
// m1igai13

#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/cdev.h>
#include <linux/fs.h>
#include <linux/slab.h>
#include <linux/uaccess.h>
#include <linux/list.h>

#define LICENCE     "GPL"
#define AUTEUR      "RAYMONDAUD Quentin"
#define DESCRIPTION "TP3"
#define DEVICE      "Linux menthe"
#define MIN(a, b)   (((a) < (b)) ? (a) : (b))
#define MAX(a, b)   (((a) > (b)) ? (a) : (b))

/* USAGE

make 
sudo su
insmod tp3.ko
mknod /dev/sm c <Major> <Minor>
dd bs=4 if=src.txt of=/dev/sm           // src.txt contient le texte source a copier
dd bs=3 if=/dev/sm of=dest.txt count=1  // dest va recevoir bloc par bloc le contenu de src qui été transmit dans /dev/sm
dmesg
rm -f /dev/sm
rmmod tp3

ex:
    make 
    sudo su
    insmod tp3.ko
    mknod /dev/sm c 240 0
    dd bs=4 if=src.txt of=/dev/sm
    dd bs=3 if=/dev/sm of=dest.txt count=1
    cat dest.txt 
    dd bs=3 if=/dev/sm of=dest.txt count=1
    cat dest.txt 
    dd bs=3 if=/dev/sm of=dest.txt count=1
    cat dest.txt 
    dd bs=3 if=/dev/sm of=dest.txt count=1
    cat dest.txt 
    dd bs=3 if=/dev/sm of=dest.txt count=1
    cat dest.txt 
    dd bs=3 if=/dev/sm of=dest.txt count=1
    cat dest.txt 
    dd bs=3 if=/dev/sm of=dest.txt count=1
    cat dest.txt
    rm -f /dev/sm
    rmmod tp3 

*/




struct CraquageDansTroisDeuxUn{
    char *maDonnee;
    ssize_t nb_donnes;
    struct list_head liste;
};

bool firstW = false;
ssize_t data_read = 0;
struct list_head maListe;




static int open(struct inode *inode, struct file *file) {
    printk(KERN_INFO "Device File Opened...!!!\n");
    firstW = true;
    return 0;
}

static ssize_t read(struct file *f, char *buf, size_t size, loff_t *offset) {
    struct CraquageDansTroisDeuxUn *unElement;
    ssize_t size_to_copy;
    ssize_t size_not_copied;
    printk(KERN_INFO " READ called \n");
    if ( list_empty(&maListe) )
        return 0;
    unElement = list_entry(maListe.next, struct CraquageDansTroisDeuxUn, liste);
    size_to_copy = MIN(size, unElement->nb_donnes - data_read );
    size_not_copied = copy_to_user(buf, unElement->maDonnee + data_read , size_to_copy * sizeof (char));
    data_read += size_to_copy - size_not_copied;
    printk(KERN_INFO " stc %ld | snc  %ld \n" , size_to_copy , size_not_copied  );
    if ( data_read == unElement->nb_donnes){
        printk(KERN_INFO " dr %ld \n" , data_read  );
        data_read = 0;
        list_del(&unElement->liste);
        kfree(unElement->maDonnee);
        kfree(unElement);
    }
    return size_to_copy - size_not_copied;
}

static ssize_t write(struct file *filp, 
                const char __user *buf, size_t len, loff_t *off)
{
    struct CraquageDansTroisDeuxUn *unElement, *nouveauCraquage;
    struct list_head  *pos, *q;
    int data_not_copied = 0;
    printk(KERN_INFO "Write Function CALLED \n");

    if ( firstW ){
        list_for_each_safe(pos, q, &maListe){
            unElement = list_entry(pos, struct CraquageDansTroisDeuxUn, liste);
            printk(KERN_INFO " Remove Elem, %ld char\n" ,unElement->nb_donnes);
            list_del(pos);
            kfree(unElement->maDonnee);
            kfree(unElement);
        }
        firstW = false;
        data_read = 0;
    }

    nouveauCraquage = (struct CraquageDansTroisDeuxUn *)kmalloc(sizeof(struct CraquageDansTroisDeuxUn),GFP_KERNEL);
    nouveauCraquage->maDonnee = (char*) kmalloc(sizeof(char) * len, GFP_KERNEL);
    list_add_tail(&nouveauCraquage->liste, &maListe);
    data_not_copied = copy_from_user(nouveauCraquage->maDonnee, buf, len);
    nouveauCraquage->nb_donnes = len - data_not_copied;
    printk(KERN_INFO " Ajout de %ld char\n", nouveauCraquage->nb_donnes);
    if (data_not_copied != 0) {
        krealloc(nouveauCraquage->maDonnee, nouveauCraquage->nb_donnes, GFP_KERNEL);
    }
    return nouveauCraquage->nb_donnes;
}
 
  
dev_t dev;
struct cdev *my_cdev = NULL;
struct file_operations fops = {
    .owner = THIS_MODULE,
    .read = read,
    .write = write,
    .open = open
};

int register_driver(void) {
    if (alloc_chrdev_region(&dev, 0, 1, "TP3") == -1) {
        printk(KERN_ALERT ">>> ERROR alloc_chrdev_region\n");
        return -1;
    }

    printk(KERN_ALERT "Init allocated (major, minor) = (%d,%d)\n", MAJOR(dev), MINOR(dev));

    my_cdev = cdev_alloc();
    if (my_cdev == NULL) {
        printk(KERN_ALERT ">>> ERROR cdev_alloc\n");
        return -2;
    }
    my_cdev->ops = &fops;
    my_cdev->owner = THIS_MODULE;

    if (cdev_add(my_cdev, dev, 1) < 0) {
        printk(KERN_ALERT ">>> ERROR cdev_add\n");
        return -3;
    }


    return 0;
}

static int d_init(void) {
    printk(KERN_ALERT " OH NON PAS LE TP3 !\n");
    INIT_LIST_HEAD(&maListe);
    if (register_driver() != 0) {
        return -1;
    }
    return 0;
}

static void d_exit(void) {
    struct CraquageDansTroisDeuxUn *unElement;
    struct list_head *pos, *q;
    printk(KERN_ALERT " ALLEE SALUTTTTT TP3 :D \n");
    list_for_each_safe(pos, q, &maListe){
        unElement = list_entry(pos, struct CraquageDansTroisDeuxUn, liste);
        printk(KERN_INFO " Remove Elem, %ld char\n" ,unElement->nb_donnes);
        list_del(pos);
        kfree(unElement->maDonnee);
        kfree(unElement);
    }
    unregister_chrdev_region(dev, 1);
    cdev_del(my_cdev);
}

MODULE_LICENSE(LICENCE);
MODULE_AUTHOR(AUTEUR);
MODULE_DESCRIPTION(DESCRIPTION);
MODULE_SUPPORTED_DEVICE(DEVICE);

module_init(d_init);
module_exit(d_exit);
