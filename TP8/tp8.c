/*
RAYMONDAUD Quentin
Compte examen : m1igai13
*/

#include <linux/init.h>
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/kernel.h>
#include <linux/cdev.h>
#include <linux/kdev_t.h>
#include <linux/fs.h>
#include <linux/uaccess.h>
#include <linux/mutex.h>
#include <linux/device.h>
#include <linux/slab.h>
#include <linux/list.h>
#include <linux/unistd.h>
#include <linux/sched.h>
#include <linux/mutex.h>

/*

Le travail a été effectué sur machine physique en installant linux source (sous linux mint)
Testée avec des scripts Python
Le mutex evite de changer de pid, donc de bufferlist courant, en cours de route : Avant que le driver soit relaché !
Il est donc possible de lancer deux fois le script sans qu'il n'y ai de problème

Update :

	// DESTRUCTION DONNEES : AJOUT DU VIDAGE DE LA LISTE DE LISTE QUI N'Y ETAIT PAS AU TP7 LORS DE LA SUPPRESSION DU DRIVER ( enlevée pour TP7 )
	// AJOUT DU MUTEX
	// Gestion ecriture / lecture partielle (Implantée depuis TP4 ou 5, supprimée pour TP7 puis remise ici)
	// Mini corrections

rm -f /dev/sr
rm -f /dev/srn
rm -f /dev/sw
rmmod tp9
make
insmod tp9.ko
mknod /dev/sr c 240 0
mknod /dev/srn c 240 1
mknod /dev/sw c 240 2
python test.py

*/

#define LICENCE     "GPL"
#define AUTEUR      "RAYMONDAUD Quentin"
#define DESCRIPTION "TP8"
#define DEVICE      "Linux menthe"
#define MIN(a, b)   (((a) < (b)) ? (a) : (b))

static DEFINE_MUTEX(mutexLock);

static ssize_t read(struct file * f, char * buf, size_t size, loff_t * offset);
static ssize_t write(struct file * f, const char * buf, size_t size, loff_t * offset);
static int s_open(struct inode * in, struct file * f);
static int release(struct inode * in, struct file * f);

// standard file_ops for char driver
static struct file_operations fops =
{
	.read = read,
	.write = write,
	.open = s_open,
	.release = release,
};

unsigned int maxFile= 3;
int debug = 0;
module_param(debug,int,S_IRUGO);

struct List_BufferList{
	struct BufferList *  BufferList;
	struct list_head node;
	struct list_head listListHead;
	int pid;
};

struct BufferList {
	char* my_data;
	unsigned int data_size;
	struct list_head node;
};

dev_t dev;
struct list_head head;
struct list_head listHead;

struct List_BufferList *  pidList;

struct cdev * my_cdev;

unsigned int size_data_read = 0;
static ssize_t read(struct file * f, char * buf, size_t size, loff_t * offset)
{
	unsigned int size_to_read;
	unsigned int size_not_read;
	struct BufferList * bufList;

	printk(KERN_ALERT "Read called !\n");
	
	if(list_empty(&head)){
		printk(KERN_ALERT "Empty List !\n");
		return 0;
	}

	bufList = list_entry(head.next, struct BufferList, node);
	size_to_read = MIN(size, bufList->data_size-size_data_read);
	size_not_read = copy_to_user(buf, bufList->my_data + size_data_read,size_to_read *  sizeof (char));
	size_data_read += size_to_read - size_not_read;

	if (debug){
		printk("bufList->my_data : %s\n", bufList->my_data);
		printk("size_to_read = %i\n", size_to_read);
		printk("size_not_read = %i\n", size_not_read);
		printk("size_data_read = %i\n", size_data_read);
	}

	if ( size_data_read == bufList->data_size){
		size_data_read = 0;
		list_del(&bufList->node);
		kfree(bufList->my_data);
		kfree(bufList);
	}
	return size_to_read - size_not_read;
}

struct list_head secureHead;
static ssize_t readN(struct file * f, char * buf, size_t size, loff_t * offset)
{
	unsigned int size_to_read;
	unsigned int size_not_read;
	struct BufferList * bufList;
	
	printk(KERN_ALERT "Read called !\n");

	if(list_empty(&head)){
		printk(KERN_ALERT "Empty List !\n");
		head = secureHead;
		return 0;
	}

	bufList = list_entry(head.next, struct BufferList, node);
	size_to_read = MIN(size, bufList->data_size-size_data_read);
	size_not_read = copy_to_user(buf, bufList->my_data + size_data_read,size_to_read *  sizeof (char));
	size_data_read += size_to_read - size_not_read;

	if (debug){
		printk("bufList->my_data : %s\n", bufList->my_data);
		printk("size_to_read = %i\n", size_to_read);
		printk("size_not_read = %i\n", size_not_read);
		printk("size_data_read = %i\n", size_data_read);
	}

	if ( size_data_read == bufList->data_size){
		size_data_read = 0;
		head = bufList->node;
	}

	return size_to_read - size_not_read;
}

static ssize_t write(struct file * f, const char * buf, size_t size, loff_t * offset)
{
	struct BufferList * bufList;
	unsigned int size_not_copied=0;
	printk(KERN_ALERT "Write called !\n");
	bufList = kmalloc(sizeof(struct BufferList), GFP_KERNEL);
	bufList->my_data = (char*) kmalloc(sizeof(char) *  size, GFP_KERNEL);
	list_add_tail(&bufList->node, &head);
	size_not_copied = copy_from_user(bufList->my_data, buf, size);

	bufList->data_size = size - size_not_copied;
	bufList->my_data[bufList->data_size]='\0';

	if (size_not_copied != 0)
		krealloc(bufList->my_data, bufList->data_size, GFP_KERNEL);

	if (debug){
		printk("size_not_copied = %d\n", size_not_copied);
		printk("bufList->my_data : %s\n", bufList->my_data);
		printk("bufList->data_size : %i\n", bufList->data_size);
	}
	return bufList->data_size;
}

static int s_open(struct inode * in, struct file * f)
{

	struct List_BufferList * bufList;
	struct list_head * bufListHead;
	struct list_head * tmp;

	if(debug)
		printk(KERN_INFO "PID bloqué : %i\n", current->pid);

	printk(KERN_ALERT "Wait Open !\n");
	mutex_lock(&mutexLock);
	printk(KERN_ALERT "Free Open !\n");

	if(debug)
		printk(KERN_INFO "PID liberé : %i\n", current->pid);
	

	if ( list_empty(&listHead)	) {
		pidList = kmalloc(sizeof(struct List_BufferList), GFP_KERNEL);
		INIT_LIST_HEAD(&pidList->node);
		list_add_tail(&pidList->node, &listHead);
		pidList->pid = current->pid;
		INIT_LIST_HEAD(&head);
		pidList->listListHead=head; //Conserve le debut de la liste pour qu'un processus avec un meme pid accède aux données 
		printk(KERN_INFO "PID %i ajouté \n", current->pid);
	}

	else{
		bool exist = false;
		list_for_each_safe(bufListHead, tmp, &listHead) {
			bufList = list_entry(bufListHead, struct List_BufferList, node);
			printk(KERN_INFO "PID reconnu : %i\n", bufList->pid);
			if (bufList->pid==current->pid){
				exist = true;
				head = bufList->listListHead;
				pidList = bufList;
			}
		}
		if (exist)
			printk(KERN_INFO "PID existant\n");
		else {
			printk(KERN_INFO "PID non existant : %i\n", current->pid);
			pidList = kmalloc(sizeof(struct List_BufferList), GFP_KERNEL);
			INIT_LIST_HEAD(&pidList->node);
			list_add_tail(&pidList->node, &listHead);
			pidList->pid = current->pid;
			INIT_LIST_HEAD(&head);
			pidList->listListHead=head;
			printk(KERN_INFO "PID %d ajouté :\n", current->pid);
		}
	}

	if (MINOR(in->i_rdev) == 0) {         	// READ
		printk(KERN_ALERT "Read\n");
		fops.read = read;
		fops.write = NULL;
	}
	
	else if (MINOR(in->i_rdev) == 1) {      // READN
		printk(KERN_ALERT "Read_NoD\n");
		secureHead = head;
		fops.read = readN;
		fops.write = NULL;
	}

	else if (MINOR (in->i_rdev) == 2) {     // WRITE
		printk(KERN_ALERT "Write\n");
		fops.read = NULL;
		fops.write = write;
	}

	else 
		printk(KERN_ALERT " Sample dafuq \n");

	return 0;
}

static int release(struct inode * in, struct file * f)
{
	pidList->listListHead = head;
	printk(KERN_ALERT "Release called\n");
	mutex_unlock(&mutexLock);
	if (debug)
		printk(KERN_ALERT "mutex unlock\n");
	return 0;
}

int d_init(void)
{
	struct mutex mutexLock;
	INIT_LIST_HEAD(&head);
	INIT_LIST_HEAD(&listHead);
	mutex_init(&mutexLock);
	if (alloc_chrdev_region(&dev,0,maxFile,"sample") == -1)
	{
		printk(KERN_ALERT ">>> ERROR alloc_chrdev_region\n");
		return -EINVAL;
	}
	printk(KERN_ALERT "Init allocated First (major, minor)=(%d,%d)\n",MAJOR(dev),MINOR(dev));
	printk(KERN_ALERT "Init allocated Last (major, minor)=(%d,%d)\n",MAJOR(dev),MINOR(dev)+maxFile-1);
	my_cdev = cdev_alloc();
	my_cdev->ops = &fops;
	my_cdev->owner = THIS_MODULE;
	cdev_add(my_cdev,dev,maxFile);
	return(0);
}


static void d_exit(void)
{
	struct BufferList * bufListItem;
	struct list_head * pos, * q;
	struct List_BufferList * listListBuf;
	struct list_head * bufListHead;
	struct list_head * tmp;
	printk(KERN_ALERT "OMG SALUT TP8 !\n");

	list_for_each_safe(bufListHead, tmp, &listHead) {
		listListBuf = list_entry(bufListHead, struct List_BufferList, node);
		head = listListBuf->listListHead;
		printk(KERN_INFO "PID supprimé : %i\n", listListBuf->pid);
		list_for_each_safe(pos, q, &head){
			bufListItem = list_entry(pos, struct BufferList, node);
			printk(KERN_INFO "Remove %i char\n" ,bufListItem->data_size);
			list_del(pos);
			kfree(bufListItem->my_data);
			kfree(bufListItem);
		}
		list_del(bufListHead);
		kfree(listListBuf);
	}
	unregister_chrdev_region(dev,maxFile);
	cdev_del(my_cdev);
}

module_exit(d_exit);
module_init(d_init);

MODULE_LICENSE(LICENCE);
MODULE_AUTHOR(AUTEUR);
MODULE_DESCRIPTION(DESCRIPTION);
MODULE_SUPPORTED_DEVICE(DEVICE);