
// Fait par Alexander Joss
// Retouché par moi même est un collègue souhaitant rester dans l'ombre


// Usage : gcc debug_tp.c -o test

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <fcntl.h>

#include <sys/ioctl.h>
#include <errno.h>

#define SAMPLE_IOC_MAGIC 'k'
#define SAMPLE_IOCRESET _IO(SAMPLE_IOC_MAGIC, 0)

#define PATH_READ_DESTRUCTIF "/dev/sr"
#define PATH_READ "/dev/srn"
#define PATH_WRITE "/dev/sw"

/*
 * lis une ligne et retourne un pointer vers cette ligne
 * le pointer doit être free
 */
char * readline_stdin() {
	int i;
	int size;
	char *buffer;
	int ch;
	i = 0;
	size = 127;
	buffer = malloc(size+1);
	ch = getchar();
	while (ch != '\n' && ch != EOF) {
		if (i == size) {
			size += 128;
			buffer = realloc(buffer, size+1);
		}
		buffer[i] = ch;
		ch = getchar();
		++i;
	}
	buffer[i] = 0;
	buffer = realloc(buffer, i+1);
	return buffer;
}


void open_read(int *fp) {
	if (*fp >= 0) {  // fichier déja ouvert
		printf("Un fichier est déja ouvert\n");
		return;
	}
	*fp = open(PATH_READ, O_RDONLY);
	if (*fp < 0)
		printf("File opening failed\n");
	else
		printf("File opened successfully\n");
}

void open_write(int *fp) {
	if (*fp >= 0) {  // fichier déja ouvert
		printf("Un fichier est déja ouvert\n");
		return;
	}
	*fp = open(PATH_WRITE, O_WRONLY | O_SYNC);
	if (*fp < 0)
		printf("File opening failed\n");
	else
		printf("File opened successfully\n");
}

void open_read_destrutrif(int *fp) {
	if (*fp >= 0) {  // fichier déja ouvert
		printf("File allready opened\n");
		return;
	}
	*fp = open(PATH_READ_DESTRUCTIF, O_RDONLY);
	if (*fp < 0)
		printf("File opening failed\n");
	else
		printf("File opened successfully\n");
}

void close_file(int *fp) {
	if (*fp < 0) {
		printf("File not opened\n");
		return;
	}
	close(*fp);
	*fp = -1;
	printf("File closed\n");
}

void close_file_ioctl(int *fp) {
	if (*fp < 0) {
		printf("File not opened\n");
		return;
	}
	/**
	int file;
  if(argc ==2){
    printf("Doing ioctl reset on %s\n",argv[1]);
    file = open(argv[1], O_RDONLY);
    if(file < 0){
      perror("open");
      printf("Error opening file %s!\n",argv[1]);
      return(errno);
    }
    ioctl(file,SAMPLE_IOCRESET,0);
    close(file);
  }
  else printf("usage: do_ioctl_reset <filename>\n");
  **/
	ioctl(*fp,SAMPLE_IOCRESET,0);
	close(*fp);
	*fp = -1;
	printf("File closed\n");
}


void read_file(int *fp, char* number) {
	int size;
	char *buf;
	int size_read;
	if (*fp < 0) {
		printf("File not opened\n");
		return;
	}
	size = atoi(number);
	buf = malloc(size + 1);
	size_read = read(*fp, buf, size);
	if (size_read == -1) {
		printf("Erreur de lecture\n");
	} else {
		buf[size_read] = 0;
		printf("read %d octets, %d octets non lu : %s\n",size_read, size-size_read, buf);
	}
	free(buf);
}

void write_file(int *fp, char* buf) {
    int size;
    int size_write;
    if (*fp < 0) {
        printf("File not opened\n");
        return;
    }
    size = strlen(buf);
    if (size <=0){
        printf("texte vide\n");
        return;
    } 
    size_write = write(*fp, buf, size);
    if (size_write == -1)
        printf("Erreur d'ecriture\n");
    else
        printf("write %d octets, %d octets non ecrit : %s\n", size_write, size-size_write, buf+size_write);
}


void syntax_error() {
	printf("Syntax :\n\
\tor : open %s en lecture\n\
\tow : open %s en ecriture\n\
\tod : open %s en lecture\n\
\tr<number> : read n octets du fichier ouvert\n\
\tw<text> : write <texte> dans le fichier ouvert\n\
\tc : close the file\n\
\ti : close the file with IOCTL\n\
\tq : quit\n", PATH_READ, PATH_WRITE, PATH_READ_DESTRUCTIF);
}

int main(int argc, char const *argv[])
{
	int fp = -1;
	char *inbuf = NULL;
	char *buffer = NULL;
	bool end = false;
	syntax_error();
	while (!end) {
		inbuf = readline_stdin();
		switch (inbuf[0]) {
		case 'r':
			read_file(&fp, inbuf + 1);
			break;
		case 'w':
			write_file(&fp, inbuf + 1);
			break;
		case 'o':
			if (inbuf[1] == 'r') {
				open_read(&fp);
			} else if (inbuf[1] == 'w' && inbuf[2] == 0) {
				open_write(&fp);
			} else if (inbuf[1] == 'd' && inbuf[2] == 0) {
				open_read_destrutrif(&fp);
			} else {
				syntax_error();
			}
			break;
		case 'c':
			if (inbuf[1] == 0) {
				close_file(&fp);
			} else {
				syntax_error();
			}
			break;
		case 'i':
			if (inbuf[1] == 0) {
				close_file_ioctl(&fp);
			} else {
				syntax_error();
			}
			break;
		case 'q':
			if (inbuf[1] == 0) {
				end = true;
			} else {
				syntax_error();
			}
			break;
		default:
			syntax_error();
			break;
		}
		free (inbuf);
	}
	if (fp >= 0) {
		printf("file closed\n");
		close(fp);
	}
	free(buffer);
	return 0;
}
