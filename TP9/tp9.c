/*
RAYMONDAUD Quentin
Compte examen : m1igai13
*/

#include <linux/init.h>
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/kernel.h>
#include <linux/cdev.h>
#include <linux/kdev_t.h>
#include <linux/fs.h>
#include <linux/uaccess.h>
#include <linux/mutex.h>
#include <linux/device.h>
#include <linux/slab.h>
#include <linux/list.h>
#include <linux/unistd.h>
#include <linux/sched.h>
#include <linux/mutex.h>
#include <linux/ioctl.h>


/*
insmod tp9.ko debug=1 => Debug display

UPDATE :
	- IOCTL
	- SOME DEBUG

USAGE : (CTRL C + CTRL V indashell =D )

rm -f /dev/sw
rm -f /dev/sr
rm -f /dev/srn
rmmod tp9

make
gcc debug_tp.c -o test
insmod tp9.ko
mknod /dev/sr c 240 0
mknod /dev/srn c 240 1
mknod /dev/sw c 240 2
./test


Example : 

	Syntax :
		or : open /dev/srn en lecture
		ow : open /dev/sw en ecriture
		od : open /dev/sr en lecture
		r<number> : read n octets du fichier ouvert
		w<text> : write <texte> dans le fichier ouvert
		c : close the file
		i : close the file with IOCTL
		q : quit
	ow
	File opened successfully
	wHate Que Ca Se Termine !
	write 24 octets, 0 octets non ecrit : 
	w Oui !
	write 6 octets, 0 octets non ecrit : 
	c
	File closed
	or
	File opened successfully
	r2
	read 2 octets, 0 octets non lu : Ha
	r2
	read 2 octets, 0 octets non lu : te
	r20 
	read 20 octets, 0 octets non lu :  Que Ca Se Termine !
	c
	File closed
	od
	File opened successfully
	r4
	read 4 octets, 0 octets non lu : Hate
	c 
	File closed
	or
	File opened successfully
	r24
	read 20 octets, 4 octets non lu :  Que Ca Se Termine !
	r10
	read 6 octets, 4 octets non lu :  Oui !
	r10
	read 10 octets, 0 octets non lu : Hate Que C
	c
	File closed
	od
	File opened successfully
	r24
	read 24 octets, 0 octets non lu : Hate Que Ca Se Termine !
	r24
	read 6 octets, 18 octets non lu :  Oui !
	r2
	read 0 octets, 2 octets non lu : 
	r2
	read 0 octets, 2 octets non lu : 
	i
	File closed
	q


*/

#define LICENCE     "GPL"
#define AUTEUR      "RAYMONDAUD Quentin"
#define DESCRIPTION "TP9"
#define DEVICE      "Linux menthe"
#define MIN(a, b)   (((a) < (b)) ? (a) : (b))
#define SAMPLE_IOC_MAGIC 'k'
#define SAMPLE_IOCRESET _IO(SAMPLE_IOC_MAGIC, 0)
#define SAMPLE_IOC_MAXNR 0

static DEFINE_MUTEX(mutexLock);

static ssize_t read(struct file * f, char * buf, size_t size, loff_t * offset);
static ssize_t write(struct file * f, const char * buf, size_t size, loff_t * offset);
static int s_open(struct inode * in, struct file * f);
static int release(struct inode * in, struct file * f);
static ssize_t ioctl (struct file * filp, unsigned int cmd, unsigned long arg);

static struct file_operations fops =
{
	.read = read,
	.write = write,
	.open = s_open,
	.release = release,
	.unlocked_ioctl= ioctl,
};

unsigned int maxFile= 3;
int debug = 0;
module_param(debug,int,S_IRUGO);

struct List_BufferList{
	struct BufferList * BufferList;
	struct list_head node;
	struct list_head listListHead;
	int pid;
};

struct BufferList {
	char * my_data;
	unsigned int data_size;
	struct list_head node;
};

dev_t dev;
struct list_head head;
struct list_head listHead;
struct List_BufferList * pidList;
struct cdev * my_cdev;
unsigned int data_read = 0;

static ssize_t read(struct file * f, char * buf, size_t size, loff_t * offset){
	unsigned int size_to_read;
	unsigned int size_not_read;
	struct BufferList * bufList;
	printk(KERN_ALERT "Read called!\n");
	if(list_empty(&head)){
		printk(KERN_ALERT "Empty!\n");
		return 0;
	}

	bufList = list_entry(head.next, struct BufferList, node);
	size_to_read = MIN(size, bufList->data_size-data_read);
	size_not_read = copy_to_user(buf, bufList->my_data + data_read,size_to_read * sizeof (char));
	data_read += size_to_read - size_not_read;

	if (debug){
		printk("size = %li\n", size);
		printk("bufList->my_data : %s\n", bufList->my_data);
		printk("size_to_read = %i\n", size_to_read);
		printk("size_not_read = %i\n", size_not_read);
		printk("data_read = %i\n", data_read);
	}

	if ( data_read == bufList->data_size){
		data_read = 0;
		list_del(&bufList->node);
		kfree(bufList->my_data);
		kfree(bufList);
	}
	pidList->listListHead = head;
	return size_to_read - size_not_read;
}

struct list_head saveHead;
static ssize_t readN(struct file * f, char * buf, size_t size, loff_t * offset){
	unsigned int size_to_read;
	unsigned int size_not_read;
	struct BufferList * bufList;
	printk(KERN_ALERT "Read called!\n");

	if(list_empty(&head)){
		printk(KERN_ALERT "Empty!\n");
		data_read=0;
		head = saveHead;
		//return 0; // Boucle Read non dest. Return si pas boucle
	}

	bufList = list_entry(head.next, struct BufferList, node);
	size_to_read = MIN(size, bufList->data_size-data_read);
	size_not_read = copy_to_user(buf, bufList->my_data + data_read,size_to_read * sizeof (char));
	data_read += size_to_read - size_not_read;

	if (debug){
		printk("size = %li\n", size);
		printk("bufList->my_data : %s\n", bufList->my_data);
		printk("size_to_read = %i\n", size_to_read);
		printk("size_not_read = %i\n", size_not_read);
		printk("data_read = %i\n", data_read);
	}

	if ( data_read == bufList->data_size){
		data_read = 0;
		head = bufList->node;
	}

	pidList->listListHead = head;
	return size_to_read - size_not_read;
}

static ssize_t write(struct file * f, const char * buf, size_t size, loff_t * offset){
	struct BufferList * bufList;
	unsigned int size_uncopied=0;
	printk(KERN_ALERT "Write called!\n");
	bufList = kmalloc(sizeof(struct BufferList), GFP_KERNEL);
	bufList->my_data = (char*) kmalloc(sizeof(char) * size, GFP_KERNEL);
	list_add_tail(&bufList->node, &head);
	size_uncopied = copy_from_user(bufList->my_data, buf, size);

	bufList->data_size = size - size_uncopied;
	bufList->my_data[bufList->data_size]='\0';

	if (size_uncopied != 0)
		krealloc(bufList->my_data, bufList->data_size, GFP_KERNEL);

	if (debug){
		printk("size_uncopied = %d\n", size_uncopied);
		printk("bufList->my_data : %s\n", bufList->my_data);
		printk("bufList->data_size : %i\n", bufList->data_size);
	}

	pidList->listListHead = head;
	return bufList->data_size;
}

static int s_open(struct inode * in, struct file * f){
	struct List_BufferList * bufList;
	struct list_head * bufListHead;
	struct list_head * tmp;

	if(debug)
		printk(KERN_INFO "PID lock : %i\n", current->pid);

	mutex_lock(&mutexLock);
	printk(KERN_ALERT "Open !\n");

	if(debug)
		printk(KERN_INFO "PID unlock : %i\n", current->pid);

	if ( list_empty(&listHead)) {
		pidList = kmalloc(sizeof(struct List_BufferList), GFP_KERNEL);
		INIT_LIST_HEAD(&pidList->node);
		list_add_tail(&pidList->node, &listHead);
		pidList->pid = current->pid;
		INIT_LIST_HEAD(&head);
		pidList->listListHead=head;
		printk(KERN_INFO "PID %i ajouté \n", current->pid);
	}

	else{
		bool exist = false;
		list_for_each_safe(bufListHead, tmp, &listHead) {
			bufList = list_entry(bufListHead, struct List_BufferList, node);
			printk(KERN_INFO "PID existant : %i\n", bufList->pid);
			if (bufList->pid==current->pid){
				exist = true;
				head = bufList->listListHead;
				pidList = bufList;
			}
		}
		if (exist)
			printk(KERN_INFO "PID existant \n");
		else{
			printk(KERN_INFO "PID non-existant : %i\n", current->pid);
			pidList = kmalloc(sizeof(struct List_BufferList), GFP_KERNEL);
			INIT_LIST_HEAD(&pidList->node);
			list_add_tail(&pidList->node, &listHead);
			pidList->pid = current->pid;
			INIT_LIST_HEAD(&head);
			pidList->listListHead=head;
			printk(KERN_INFO "PID %d ajouté\n", current->pid);
		}
	}

	if (MINOR( in->i_rdev) == 0){
		printk(KERN_ALERT "Read !\n");
		fops.read = read;
		fops.write = NULL;
	}

	else if (MINOR( in->i_rdev) == 1){
		printk(KERN_ALERT "ReadN !\n");
		saveHead = head;
		fops.read = readN;
		fops.write = NULL;
	}

	else if (MINOR( in->i_rdev) == 2){
		printk(KERN_ALERT "Write !\n");
		fops.read = NULL;
		fops.write = write;
	}
	
	else
		printk(KERN_ALERT "WatDaFuq\n");

	return 0;
}

static int release(struct inode * in, struct file * f) {
	if (MINOR( in->i_rdev) == 1){
		printk(KERN_ALERT "Data read = 0 \n");
		data_read=0;
		head = saveHead;
	}
	pidList->listListHead = head;

	printk(KERN_ALERT "Release called\n");
	mutex_unlock(&mutexLock);
	if (debug)
		printk(KERN_ALERT "Mutex unlock\n");
	
	return 0;
}

static ssize_t ioctl (struct file * filp, unsigned int cmd, unsigned long arg) {
	struct BufferList * bufListItem;
	struct list_head * pos, * q;
	struct List_BufferList * listListBuf;
	struct list_head * bufListHead;
	struct list_head * tmp;
	printk(KERN_ALERT "ioctl called!\n");

	// Vérif valide
	// sinon : ENOTTY
	switch(cmd) {
		case SAMPLE_IOCRESET: // Destruction données 
			printk(KERN_INFO "PID IOCTL : %i\n", current->pid);
			list_for_each_safe(bufListHead, tmp, &listHead) {
				listListBuf = list_entry(bufListHead, struct List_BufferList, node);
				if (listListBuf->pid == current->pid){
					printk(KERN_INFO "PID buf : %i\n", listListBuf->pid);
					head = listListBuf->listListHead;
					if(list_empty(&head))
						printk(KERN_ALERT "Empty!\n");
					else{
						list_for_each_safe(pos, q, &head){
							bufListItem = list_entry(pos, struct BufferList, node);
							printk(KERN_INFO "Remove Elem, %s " ,bufListItem->my_data);
							printk(KERN_INFO " = %i char\n" ,bufListItem->data_size);
							list_del(pos);
							kfree(bufListItem->my_data);
							kfree(bufListItem);
						}
					}
					list_del(bufListHead);
					kfree(listListBuf);
					break;
				}
			}
			break;
		default:
			return -ENOTTY;
		}
		return 0;
	}

int d_init(void)
{
	struct mutex mutexLock;
	INIT_LIST_HEAD(&head);
	INIT_LIST_HEAD(&listHead);
	mutex_init(&mutexLock);
	
	if (alloc_chrdev_region(&dev,0,maxFile,"sample") == -1)
	{
		printk(KERN_ALERT ">>> ERROR alloc_chrdev_region\n");
		return -EINVAL;
	}
	printk(KERN_ALERT "Init allocated First (major, minor)=(%d,%d)\n",MAJOR(dev),MINOR(dev));
		printk(KERN_ALERT "Init allocated Last (major, minor)=(%d,%d)\n",MAJOR(dev),MINOR(dev)+maxFile-1);

	my_cdev = cdev_alloc();
	my_cdev->ops = &fops;
	my_cdev->owner = THIS_MODULE;
	cdev_add(my_cdev,dev,maxFile);

	return(0);
}


static void d_exit(void)
{
	struct BufferList * bufListItem;
	struct list_head * pos, * q;
	struct List_BufferList * listListBuf;
	struct list_head * bufListHead;
	struct list_head * tmp;
	printk(KERN_ALERT "OMFG CIAOOOOOOOOOOO :D \n");

	list_for_each_safe(bufListHead, tmp, &listHead) {
		listListBuf = list_entry(bufListHead, struct List_BufferList, node);
		head = listListBuf->listListHead;
		printk(KERN_INFO "PID supprimé : %i\n", listListBuf->pid);
		list_for_each_safe(pos, q, &head){
			bufListItem = list_entry(pos, struct BufferList, node);
			printk(KERN_INFO "Remove Elem, %i char\n" ,bufListItem->data_size);
			list_del(pos);
			kfree(bufListItem->my_data);
			kfree(bufListItem);
		}
		list_del(bufListHead);
		kfree(listListBuf);
	}

	unregister_chrdev_region(dev,maxFile);
	cdev_del(my_cdev);
}

module_exit(d_exit);
module_init(d_init);

MODULE_LICENSE(LICENCE);
MODULE_AUTHOR(AUTEUR);
MODULE_DESCRIPTION(DESCRIPTION);
MODULE_SUPPORTED_DEVICE(DEVICE);
